##Created by Elliot Jerge April 2019##
Write-Host "This is a Extremely Early Alpha Script"


$Title = "M365 Administration"
$Info = "What would you like to do?"
$creds = $host.ui.PromptForCredential("Microsoft Tenant Credentials", "Please enter your user name and password for a desired tenant", "", "NetBiosUserName") 


<#Define What I want This Script to Do
    1. Licensing
    2. User Management
    3. Mailbox Rules/Forwarding
    4. Security/Compliance
    #>
    $menu=@"
    1. Licensing
    2. User Management
    3. Exchange 
    4. Security and Compliance
    Q. Quit Script
"@
    Write-Host "O365 Admin" -ForegroundColor DarkMagenta
    $r = Read-Host $menu

    if ($r -eq 1){
    $menu1=@"
    1. Add a License
    2. Remove a License
    3. See all Licenses in Organization


"@
    Write-Host "Licensing Menu" -ForegroundColor DarkMagenta
    $r1 = Read-Host $menu1


}

    if ($r -eq 2){
    $menu2=@"
    1. Add a User
    2. Delete a User
    3. Block a User
    4. Add a User Role
    5. Remove a User Role


"@
    Write-Host "User Management Menu" -ForegroundColor DarkMagenta
    $r2 = Read-Host $menu2

    }


    if ($r -eq 3){
    $menu3=@"
    1. Convert To Shared Mailbox
    2. Perform a Message Trace
    3. 
    4. 


"@

        Write-Host "Exchange Management Menu" -ForegroundColor DarkMagenta
        $r3 = Read-Host $menu3

    }
                                       
 ##License Management
<#$options = [System.Management.Automation.Host.ChoiceDescription[]] 
@("&Add User License", "&Remove User License", "&List all Licenses in Tenant", "&Create a User")
[int]$defaultchoice = 2
$opt = $host.UI.PromptForChoice($Title , $Info , $Options,$defaultchoice)
switch($opt)
{
0 { Write-Host "Add User License" -ForegroundColor Green}
1 {Write-Host "Remove User License" -ForegroundColor Green}
2 {Write-Host "List all Licenses in Tenant" -ForegroundColor Green}
3 {Write-Host "Create a User" -Foreground Color Green}
}
#>

<#
    Licensing Scripts
#>
if ($r1 -eq 1){


$UPN = Read-Host "Enter User Principal Name: "
$License = Read-Host "Enter Desired License to Add: "
$tenantname = Read-Host "Enter the Tenant you want to Connect To: "


Connect-MsolService -Credential $creds
Set-MsolUserLicense -UserPrincipalName $UPN -AddLicenses winonait:$license
}


if($r1 -eq 2){

$UPN = Read-Host "Enter UPN: "
$License = Read-Host "Enter Desired License to Remove: "
$tenantname = Read-Host

Connect-MsolService -Credential $creds
Set-MsolUserLicense -UserPrincipalName $UPN -RemoveLicenses winonait:$license
Write-Host "Would you like to be do anything else?" -ForegroundColor DarkYellow
    $Readhost = Read-Host " (Y/N) "
    Switch ($Readhost)
    {
        Y {Write-Host "Going Back to Main Menu"; $true}
        N {Write-Host "Exiting Script"; $false}
    }

    if($readhost -eq "Y"){
        Exit-PSHostProcess
    }
}

elseif($r1 -eq 2){



}


##User Management
if($r2 -eq 1){
##Add A User
Connect-MsolService


}



##Mailbox Management
if($r3 -eq 2){
    $Session = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri https://outlook.office365.com/powershell-liveid/ -Credential $creds -Authentication Basic -AllowRedirection
    Import-PSSession $Session -DisableNameChecking
    Get-MessageTrace -SenderAddress elliot.winonait@fellowshipmissions.net 

}




##Security Management
