#loads in CSV File
$user_file = Import-CSV 'C:\users.csv'

#Modifies all AzureAD User Properties based on Inclusion in CSV
$user_file | ForEach {Set-AzureADUser -ObjectID $_.ObjectID -State $_.State -JobTitle $_.Title -Department $_.class}
$user_file | ForEach {Set-AzureADUserManager -ObjectID $_.ObjectID -RefObjectID $_.Supervisor} 