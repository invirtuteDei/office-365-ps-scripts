## Script Used for Configuring VBO Service Account for Veeam
$upn = Read-Host("Enter your Office Login")
$veeamaccount = Read-Host("Enter Veeam Service Account Name (ex. Veeam Admin")
$veeamemail = Read-Host("Enter Veeam Email Address")
Connect-ExchangeOnline -UserPrincipalName $upn -ShowProgress $true


## Exchange Online Config
New-AuthenticationPolicy -Name .
New-AuthenticationPolicy -Name Veeam
Set-AuthenticationPolicy -Identity “Veeam” -AllowBasicAuthPowershell
Set-AuthenticationPolicy -Identity “Veeam” -AllowBasicAuthWebServices
Set-User -Identity $veeamaccount -AuthenticationPolicy “Veeam”
New-ManagementRoleAssignment -Role:ApplicationImpersonation -User: $veeamemail
New-ManagementRoleAssignment -Role:”View-Only Configuration” -User: $veeamemail
New-ManagementRoleAssignment -Role:”View-Only Recipients” -User: $veeamemail
New-ManagementRoleAssignment -Role:”Mailbox Search” -User: $veeamemail

