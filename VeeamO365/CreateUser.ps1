##Creating Veeam Service Account
##Author: Elliot J.
##Date: Spring 2020

Install-Module PowershellGet -Force
Install-Module MSOnline

##Define Variables for Creation
$displayname = Read-Host("Enter Display Name")
$firstname = Read-Host("Enter First Name")
$lastname = Read-Host("Enter Last Name")
$upn = Read-Host("Enter Email Address")
$pass = Read-Host("Enter a Password") -AsSecureString
#Initialize Module
Connect-MsolService

#Create User
New-MsolUser -DisplayName $displayname -FirstName $firstname -LastName $lastname -UserPrincipalName $upn -UsageLocation US -Password $pass


#Enable MFA Requirements
$mf= New-Object -TypeName Microsoft.Online.Administration.StrongAuthenticationRequirement
$mf.RelyingParty = "*"
$mfa = @($mf)
#Enable MFA for a user
Set-MsolUser -UserPrincipalName $upn -StrongAuthenticationRequirements $mfa
Write-Host("Please Sign into the Account to Retreive the App Password")

#Assign Admin Roles
$displayname
$rolename = "Exchange Administrator"
$rolename2 = "SharePoint Service Administrator"
Add-MsolRoleMember -RoleMemberEmailAddress (Get-MsolUser -All | Where DisplayName -eq $displayname).UserPrincipalName -RoleName $rolename, $rolename2
Write-Host("You created a User with the Following")
Write-Host $displayname
Write-Host $firstname
Write-Host $lastname
Write-Host $upn
Write-Host("You assigned it the Exchange and Sharepoint Administrator Roles, Please Sign-In to Obtain App Password")













