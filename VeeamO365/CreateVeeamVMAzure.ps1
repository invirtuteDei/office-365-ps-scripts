## This Script is used to Create a Veeam O365 VM in Azure
#Install Azure Module If not already Installed
Install-Module -Name Az -AllowClobber -Scope CurrentUser
#Sign into Azure
Connect-AzureAD

#Create Resource Group
$rg = Read-Host("What would you like the Resource Group Name to be")
New-AzResourceGroup -Name $rg -Location NorthCentralUS 

#Create Virtual Machine
#Define Variable
$vmname = Read-Host("What is the VM Name?")
$rg

New-AzVm `
    -ResourceGroupName $rg
    -Name $vmname
    -Location "North Central US"
    -VirtualNetworkName "VeeamVnet"
    -SubnetName "VeaamSubnet"
    -SecurityGroupName "myNetworkSecurityGroup"
    -PublicIPAddressName "myPublicIPAddress"
    -OpenPorts 3389